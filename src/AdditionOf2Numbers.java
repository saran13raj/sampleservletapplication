

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdditionOf2Numbers
 */
@WebServlet("/AdditionOf2Numbers")
public class AdditionOf2Numbers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdditionOf2Numbers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pr=response.getWriter();// line 10
        response.setContentType("text/html");
        try
        {
           int num1=Integer.parseInt(request.getParameter("num1"));
           int num2=Integer.parseInt(request.getParameter("num2"));
           int sum=num1+num2;
           pr.println("<html><body><center>");
           pr.println("Number 1<input type=text value=" +num1 +"><br>");
           pr.println("Number 2<input type=text value=" +num2 +"><br>");
           pr.println("Total is " + sum);
//           pr.println("Output<input type=text value=" +sum +">");
           pr.println("</center></body></html>");
        }
        catch(Exception e)
        {
        pr.println("Invalid Input");
        }
	}

}
